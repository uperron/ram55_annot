import re
import numpy as np
import pandas as pd
import warnings
import requests
import json
from ruamel.yaml import YAML
yaml = YAML()

# various I/O functions for common formats

def read_fastafile(fastafile):
    arr = []
    row = []
    seq = []
    with open(fastafile) as fasta_file:
        for line in fasta_file:
            if line.startswith(">"):
                row.append(seq)
                arr.append(row)
                seq = []
                row = line.strip(">\n").split(";")
            elif ":" in line:
                # this metadata line has some inconsistencies in a few families
                row.extend([s if s != ' ' else "NA" for s in line.strip("\n").split(":")])
            else:
                seq.extend(line.strip("\s\n*\r"))
        row.append(seq)
        arr.append(row)
    return pd.DataFrame(arr)

def url_response(url):
    try:
        r = requests.get(url=url)
    except:
        return None
    if r.status_code == 200:
        json_result = r.json()
        return json_result
    else:
        return None

def slice_residue_range(all_residues, res_range):
    return [(r, r.get("residue_number")) for r in all_residues if r.get("residue_number") in res_range]


def get_sequences(pdb_id, entity_id, pdb_chain_id, start_res, end_res, res_dict, pen_2_dum_df):
    # rama_search_url = "https://www.ebi.ac.uk/pdbe/api/validation/rama_sidechain_listing/entry/"  + pdb_id
    # internal EBI VM url
    rama_search_url = "http://ves-oy-89.ebi.ac.uk/pdbe/api/validation/rama_sidechain_listing/entry/" + pdb_id
    results = url_response(rama_search_url)
    # check query result
    if results is None:
        warnings.warn("Failed rama_sidechain_listing query for {}".format(pdb_id))
        return
    for entity in results[pdb_id.lower()]["molecules"]:
        if entity.get('entity_id') == entity_id:
            for chain in entity.get("chains"):
                if chain.get("chain_id") == pdb_chain_id: # FILTER ON CHAIN_ID
                    for model in chain.get("models"):
                        seq_id = "_".join([str(e) for e in [pdb_id, entity_id, 
                                                            pdb_chain_id, start_res, end_res]])
                        rota_seq = []
                        aa_seq = []
                        res_nums = []
                        all_residues = model["residues"]
                        res_range = list(range(int(start_res), int(end_res) + 1))
                        # RESIDUE NUMBER FILTER
                        for residue, residue_number in slice_residue_range(all_residues, res_range):
                            aa_state = residue.get("residue_name")
                            res_nums.append(residue_number)
                            if residue.get("residue_name") in ['ALA', 'GLY']:
                                rota_seq.append(aa_state)
                                aa_seq.append(aa_state)
                            else:
                                if residue.get("rama") == "Favored" or residue.get("rama") == "Allowed":
                                    # if the residue's rotamer configuration can be determined
                                    res_name = residue.get("residue_name")
                                    res_pen_rota = residue.get("rota") 
                                    try:
                                        res_name_long = res_dict[res_name]
                                        rota_seq.append(pen_2_dum_df.loc[res_name_long].loc[res_pen_rota, 
                                        "Dunbrack_typical_state"])
                                        aa_seq.append(aa_state)
                                    except KeyError:
                                        # non-standard residues (e.g. MSE), undetermined rotamers (OUTLIER, None, mtmp?)
                                        warnings.warn('Could not translate {} for amino acid {}'.format(res_pen_rota, 
                                        res_name))
                                        rota_seq.append("-")
                                        aa_seq.append(aa_state)
                                else:
                                        rota_seq.append("-")
                                        aa_seq.append(aa_state)
                        return (aa_seq, rota_seq, res_nums, [pdb_chain_id]*len(res_nums))

# Relative accessibility
def calc_rasa(acc, resname, residue_max_acc):
    try:
        rel_acc = acc / residue_max_acc[resname]
    except KeyError:
        # Invalid value for resname
        rel_acc = 'NA'
    return rel_acc

def auth2idx(vec, resnum_df):
    # convert from pdb residue num and chain id to
    # author residue num and chain id
    # dssp and biopython use the latter
    ch, idx = vec
    idx = str(int(idx))
    try:
        auth_vec = resnum_df.query("pdb_ch_id == @ch  & pdb_res_num == @idx")[["auth_ch_id", 
        "auth_res_num"]].values[0]
    except:
        auth_vec = vec
    key = (auth_vec[0], (' ', int(auth_vec[1]), ' '))
    return key

def read_dssp(vec, dssp_dict, resnum_df, residue_max_acc):
    key = auth2idx(vec, resnum_df)
    try:
        (aa, ss, acc, phi, psi, dssp_index,
        NH_O_1_relidx, NH_O_1_energy,
        O_NH_1_relidx, O_NH_1_energy,
        NH_O_2_relidx, NH_O_2_energy,
        O_NH_2_relidx, O_NH_2_energy) = dssp_dict[key]
    except KeyError:
        print('KEY ERROR %s' % (str(key)))
        return ["NA"] * 10

    rasa = calc_rasa(acc, aa, residue_max_acc)    
    return [aa, ss, acc, rasa, phi, psi, NH_O_1_energy,
    O_NH_1_energy, NH_O_2_energy, O_NH_2_energy]

# Half Sphere Exposure, a 2D measure of solvent exposure. 
def read_HSE(vec, resnum_df, HSE_ca, eCN):
    key = auth2idx(vec, resnum_df)
    try:
        ca = HSE_ca[key][2]
    except KeyError:
        print('HSE_ca KEY ERROR %s' % (str(key)))
        ca = 'NA'
    try:
        CN = eCN[key]
    except KeyError:
        print('eCN KEY ERROR %s' % (str(key)))
        CN = 'NA'
    return [ca, CN]
